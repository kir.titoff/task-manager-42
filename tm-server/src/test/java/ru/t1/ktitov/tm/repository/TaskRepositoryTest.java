package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.ITaskRepository;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.dto.model.TaskDTO;
import ru.t1.ktitov.tm.service.ConnectionService;
import ru.t1.ktitov.tm.service.PropertyService;

import java.util.List;

import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.TaskTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private void compareTasks(@NotNull final TaskDTO task1, @NotNull final TaskDTO task2) {
        Assert.assertEquals(task1.getId(), task2.getId());
        Assert.assertEquals(task1.getName(), task2.getName());
        Assert.assertEquals(task1.getDescription(), task2.getDescription());
        Assert.assertEquals(task1.getStatus(), task2.getStatus());
        Assert.assertEquals(task1.getUserId(), task2.getUserId());
        Assert.assertEquals(task1.getProjectId(), task2.getProjectId());
        Assert.assertEquals(task1.getCreated(), task2.getCreated());
    }

    private void compareTasks(
            @NotNull final List<TaskDTO> taskList1,
            @NotNull final List<TaskDTO> taskList2) {
        Assert.assertEquals(taskList1.size(), taskList2.size());
        for (int i = 0; i < taskList1.size(); i++) {
            compareTasks(taskList1.get(i), taskList2.get(i));
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.clearTasksByUser(USER1.getId());
            repository.clearTasksByUser(USER2.getId());
            repository.clearTasksByUser(ADMIN3.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            Assert.assertTrue(repository.findAllTasks().isEmpty());
            repository.addTask(USER1_TASK1);
            sqlSession.commit();
            TaskDTO task = repository.findAllTasks().get(0);
            compareTasks(USER1_TASK1, task);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void addList() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            Assert.assertTrue(repository.findAllTasks().isEmpty());
            repository.addTask(USER1_TASK1);
            repository.addTask(USER1_TASK2);
            repository.addTask(USER1_TASK3);
            Assert.assertEquals(3, repository.getTasksSize());
            compareTasks(USER1_TASK1, repository.findAllTasks().get(0));
            compareTasks(USER1_TASK2, repository.findAllTasks().get(1));
            compareTasks(USER1_TASK3, repository.findAllTasks().get(2));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.addTask(USER1_TASK1);
            compareTasks(USER1_TASK1, repository.findAllTasks().get(0));
            repository.clearTasks();
            Assert.assertTrue(repository.findAllTasks().isEmpty());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.addTask(USER1_TASK1);
            repository.addTask(USER1_TASK2);
            repository.addTask(USER1_TASK3);
            repository.clearTasksByUser(USER2.getId());
            Assert.assertFalse(repository.findAllTasks().isEmpty());
            repository.clearTasksByUser(USER1.getId());
            Assert.assertTrue(repository.findAllTasks().isEmpty());
            repository.addTask(USER1_TASK1);
            repository.clearTasksByUser(USER2.getId());
            compareTasks(USER1_TASK1, repository.findAllTasks().get(0));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.addTask(USER1_TASK1);
            repository.addTask(USER1_TASK2);
            repository.addTask(USER1_TASK3);
            repository.addTask(USER2_TASK1);
            compareTasks(USER1_TASK_LIST, repository.findAllTasksByUser(USER1.getId()));
            compareTasks(USER2_TASK_LIST, repository.findAllTasksByUser(USER2.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.addTask(USER1_TASK1);
            repository.addTask(USER1_TASK2);
            repository.addTask(USER1_TASK3);
            compareTasks(USER1_TASK1, repository.findTaskById(USER1_TASK1.getId()));
            Assert.assertNull(repository.findTaskByIdByUser(USER2.getId(), USER1_TASK1.getId()));
            compareTasks(USER1_TASK1, repository.findTaskByIdByUser(USER1.getId(), USER1_TASK1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.addTask(USER1_TASK1);
            repository.addTask(USER1_TASK2);
            repository.addTask(USER1_TASK3);
            repository.removeTask(USER1_TASK1);
            Assert.assertEquals(2, repository.getTasksSize());
            repository.removeTaskById(USER1_TASK2.getId());
            Assert.assertEquals(1, repository.getTasksSize());
            compareTasks(USER1_TASK3, repository.findAllTasks().get(0));
            repository.clearTasks();
            repository.addTask(USER1_TASK1);
            repository.addTask(USER1_TASK2);
            repository.addTask(USER1_TASK3);
            Assert.assertEquals(3, repository.getTasksSize());
            repository.removeTaskByIdByUser(USER2.getId(), USER1_TASK1.getId());
            Assert.assertEquals(3, repository.getTasksSize());
            repository.removeTaskByIdByUser(USER1.getId(), USER1_TASK1.getId());
            repository.removeTaskByIdByUser(USER1.getId(), USER1_TASK2.getId());
            compareTasks(USER1_TASK3, repository.findAllTasks().get(0));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.addTask(USER1_TASK1);
            repository.addTask(USER1_TASK2);
            repository.addTask(USER1_TASK3);
            repository.addTask(USER2_TASK1);
            compareTasks(USER1_TASK_LIST, repository.findAllTasksByProjectId(USER1.getId(), USER1_PROJECT1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
