package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.dto.model.UserDTO;
import ru.t1.ktitov.tm.service.ConnectionService;
import ru.t1.ktitov.tm.service.PropertyService;
 
import java.util.List;

import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private void compareUsers(@NotNull final UserDTO user1, @NotNull final UserDTO user2) {
        Assert.assertEquals(user1.getId(), user2.getId());
        Assert.assertEquals(user1.getLogin(), user2.getLogin());
        Assert.assertEquals(user1.getPasswordHash(), user2.getPasswordHash());
        Assert.assertEquals(user1.getEmail(), user2.getEmail());
        Assert.assertEquals(user1.getFirstName(), user2.getFirstName());
        Assert.assertEquals(user1.getLastName(), user2.getLastName());
        Assert.assertEquals(user1.getMiddleName(), user2.getMiddleName());
        Assert.assertEquals(user1.getRole(), user2.getRole());
        Assert.assertEquals(user1.getLocked(), user2.getLocked());
    }

    private void compareUsers(
            @NotNull final List<UserDTO> userList1,
            @NotNull final List<UserDTO> userList2) {
        Assert.assertEquals(userList1.size(), userList2.size());
        for (int i = 0; i < userList1.size(); i++) {
            compareUsers(userList1.get(i), userList2.get(i));
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.deleteUser(USER1.getId());
            repository.deleteUser(USER2.getId());
            repository.deleteUser(ADMIN3.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            Assert.assertTrue(repository.findAllUsers().isEmpty());
            repository.addUser(USER1);
            compareUsers(USER1, repository.findAllUsers().get(0));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void addList() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            Assert.assertTrue(repository.findAllUsers().isEmpty());
            repository.addUser(USER1);
            repository.addUser(USER2);
            repository.addUser(ADMIN3);
            Assert.assertEquals(3, repository.getUsersSize());
            compareUsers(USER_LIST1, repository.findAllUsers());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.addUser(USER1);
            repository.addUser(USER2);
            repository.addUser(ADMIN3);
            Assert.assertEquals(3, repository.getUsersSize());
            repository.clearUsers();
            Assert.assertTrue(repository.findAllUsers().isEmpty());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllUsers() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.addUser(USER1);
            repository.addUser(USER2);
            repository.addUser(ADMIN3);
            compareUsers(USER_LIST1, repository.findAllUsers());
            repository.clearUsers();
            Assert.assertTrue(repository.findAllUsers().isEmpty());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.addUser(USER1);
            repository.addUser(USER2);
            repository.addUser(ADMIN3);
            compareUsers(USER1, repository.findUserById(USER1.getId()));
            compareUsers(USER2, repository.findUserById(USER2.getId()));
            compareUsers(ADMIN3, repository.findUserById(ADMIN3.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.addUser(USER1);
            repository.addUser(USER2);
            repository.addUser(ADMIN3);
            Assert.assertEquals(3, repository.getUsersSize());
            repository.removeUser(USER1);
            Assert.assertEquals(2, repository.getUsersSize());
            repository.removeUser(USER4);
            Assert.assertEquals(2, repository.getUsersSize());
            repository.removeUserById(USER2.getId());
            Assert.assertEquals(1, repository.getUsersSize());
            compareUsers(ADMIN3, repository.findAllUsers().get(0));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByLogin() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.addUser(USER1);
            repository.addUser(USER2);
            repository.addUser(ADMIN3);
            compareUsers(USER1, repository.findUserByLogin("user1"));
            compareUsers(USER2, repository.findUserByLogin("user2"));
            compareUsers(ADMIN3, repository.findUserByLogin("user3"));
            Assert.assertNull(repository.findUserByLogin("admin1"));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByEmail() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.addUser(USER1);
            repository.addUser(USER2);
            repository.addUser(ADMIN3);
            compareUsers(USER1, repository.findUserByEmail("user1@gmail.com"));
            compareUsers(USER2, repository.findUserByEmail("user2@gmail.com"));
            compareUsers(ADMIN3, repository.findUserByEmail("user3@gmail.com"));
            Assert.assertNull(repository.findUserByEmail("admin1@gmail.com"));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
