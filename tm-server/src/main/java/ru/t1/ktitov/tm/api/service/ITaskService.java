package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    Collection<TaskDTO> set(@NotNull Collection<TaskDTO> tasks);

    @Nullable
    TaskDTO add(@Nullable TaskDTO task);

    @Nullable
    TaskDTO add(@Nullable String userId, @Nullable TaskDTO task);

    @NotNull
    Collection<TaskDTO> add(@NotNull Collection<TaskDTO> tasks);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void clear();

    void clear(@Nullable String userId);

    @Nullable
    TaskDTO remove(@Nullable TaskDTO task);

    @Nullable
    TaskDTO remove(@Nullable String userId, @Nullable TaskDTO task);

    @Nullable
    TaskDTO removeById(@Nullable String id);

    @Nullable
    TaskDTO removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    @Nullable
    List<TaskDTO> findAll(@Nullable Sort sort);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    TaskDTO findOneById(@Nullable String id);

    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    int getSize(@Nullable String userId);

    int getSize();

}
