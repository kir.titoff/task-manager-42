package ru.t1.ktitov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.ISessionRepository;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.ISessionService;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.field.EmptyIdException;
import ru.t1.ktitov.tm.exception.field.EmptyUserIdException;
import ru.t1.ktitov.tm.dto.model.SessionDTO;

import java.util.Collection;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    protected final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<SessionDTO> set(@Nullable Collection<SessionDTO> sessions) {
        if (sessions == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.clearSessions();
            sessions.forEach(repository::addSession);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return sessions;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO add(@Nullable final SessionDTO session) {
        if (session == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.addSession(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO add(@Nullable final String userId, @Nullable final SessionDTO session) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (session == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            session.setUserId(userId);
            repository.addSession(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<SessionDTO> add(@Nullable Collection<SessionDTO> sessions) {
        if (sessions == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessions.forEach(repository::addSession);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return sessions;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.clearSessions();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.clearSessionsByUser(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO remove(@Nullable final SessionDTO session) {
        if (session == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeSession(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO remove(@Nullable final String userId, @Nullable final SessionDTO session) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (session == null) throw new EntityNotFoundException();
        if (!userId.equals(session.getUserId())) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeSession(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final SessionDTO session;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            session = repository.findSessionById(id);
            repository.removeSessionById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final SessionDTO session;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            session = findOneById(userId, id);
            repository.removeSessionByIdByUser(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.findAllSessions();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.findAllSessionsByUser(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.findAllSessionsWithOrder(sort.getDisplayName());
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.findAllSessionsByUserWithOrder(userId, sort.getDisplayName());
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.findSessionById(id) != null;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.findSessionByIdByUser(userId, id) != null;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            @Nullable final SessionDTO session = repository.findSessionById(id);
            if (session == null) throw new EntityNotFoundException();
            return session;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            @Nullable final SessionDTO session = repository.findSessionByIdByUser(userId, id);
            if (session == null) throw new EntityNotFoundException();
            return session;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.getSessionsSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.getSessionsSizeByUser(userId);
        } finally {
            sqlSession.close();
        }
    }

}
