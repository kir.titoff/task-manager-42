package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> projects);

    @Nullable
    ProjectDTO add(@Nullable ProjectDTO project);

    @Nullable
    ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO project);

    @NotNull
    Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void clear();

    void clear(@Nullable String userId);

    @Nullable
    ProjectDTO remove(@Nullable ProjectDTO project);

    @Nullable
    ProjectDTO remove(@Nullable String userId, @Nullable ProjectDTO project);

    @Nullable
    ProjectDTO removeById(@Nullable String id);

    @Nullable
    ProjectDTO removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable
    List<ProjectDTO> findAll(@Nullable Sort sort);

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    ProjectDTO findOneById(@Nullable String id);

    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    int getSize();

    int getSize(@Nullable String userId);

}
