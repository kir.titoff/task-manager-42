package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.dto.model.SessionDTO;

import java.util.Collection;
import java.util.List;

public interface ISessionService {

    @NotNull
    Collection<SessionDTO> set(@NotNull Collection<SessionDTO> sessions);

    @Nullable
    SessionDTO add(@Nullable SessionDTO session);

    @Nullable
    SessionDTO add(@Nullable String userId, @Nullable SessionDTO session);

    @NotNull
    Collection<SessionDTO> add(@NotNull Collection<SessionDTO> sessions);

    void clear();

    void clear(@Nullable String userId);

    @Nullable
    SessionDTO remove(@Nullable SessionDTO session);

    @Nullable
    SessionDTO remove(@Nullable String userId, @Nullable SessionDTO session);

    @Nullable
    SessionDTO removeById(@Nullable String id);

    @Nullable
    SessionDTO removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    List<SessionDTO> findAll(@Nullable String userId);

    @Nullable
    List<SessionDTO> findAll(@Nullable Sort sort);

    @Nullable
    List<SessionDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    SessionDTO findOneById(@Nullable String id);

    SessionDTO findOneById(@Nullable String userId, @Nullable String id);

    int getSize();

    int getSize(@Nullable String userId);

}
