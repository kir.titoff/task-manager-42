package ru.t1.ktitov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.IProjectRepository;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IProjectService;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.field.*;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    protected final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<ProjectDTO> set(@Nullable Collection<ProjectDTO> projects) {
        if (projects == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.clearProjects();
            projects.forEach(repository::addProject);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO add(@Nullable final ProjectDTO project) {
        if (project == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.addProject(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO add(@Nullable final String userId, @Nullable final ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            project.setUserId(userId);
            repository.addProject(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<ProjectDTO> add(@Nullable Collection<ProjectDTO> projects) {
        if (projects == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projects.forEach(repository::addProject);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return projects;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.addProject(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.addProject(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.updateProject(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setStatus(status);
        project.setUserId(userId);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.updateProject(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.clearProjects();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.clearProjectsByUser(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO remove(@Nullable final ProjectDTO project) {
        if (project == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.removeProject(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO remove(@Nullable final String userId, @Nullable final ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EntityNotFoundException();
        if (!userId.equals(project.getUserId())) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.removeProject(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final ProjectDTO project;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            project = repository.findProjectById(id);
            repository.removeProjectById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final ProjectDTO project;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            project = findOneById(userId, id);
            repository.removeProjectByIdByUser(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.findAllProjects();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.findAllProjectsByUser(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.findAllProjectsWithOrder(sort.getDisplayName());
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.findAllProjectsByUserWithOrder(userId, sort.getDisplayName());
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.findProjectById(id) != null;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.findProjectByIdByUser(userId, id) != null;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable final ProjectDTO project = repository.findProjectById(id);
            if (project == null) throw new EntityNotFoundException();
            return project;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable final ProjectDTO project = repository.findProjectByIdByUser(userId, id);
            if (project == null) throw new EntityNotFoundException();
            return project;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.getProjectsSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.getProjectsSizeByUser(userId);
        } finally {
            sqlSession.close();
        }
    }

}
